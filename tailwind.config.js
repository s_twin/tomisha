module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./nuxt.config.{js,ts}",
  ],
  theme: {
    extend: {
      colors: {
        'main-green-pale': '#E6FFFA',
        'main-green': '#81E6D9',
        'main-green-chromatic': '#319795',
        'main-blue': '#3182CE',
        'main-blue-navy': '#EBF4FF',
        'main-black': '#2D3748',
        'main-white-transparent': '#00000029',
        'main-white-so-transparent': '#00000033',
        'main-brown': '#707070',
        'main-bit-white': '#F7FAFC',
        'main-navy-pale': '#CBD5E0',
        'main-navy': '#718096',
        'main-navy-chromatic': '#4A5568',
      },
      keyframes: {
        'center-left': {
          '0%': { "transform": "translate(-30px, 0px)" },
          '100%': { "transform": "translate(50px, 0px)" },
        },
        'right-left': {
          '0%': { "transform": "translate(-140px, 0px)" },
          '100%': { "transform": "translate(50px, 0px)" },
        },
        'left-center': {
          '0%': { "transform": "translate(50px, 0px)" },
          '100%': { "transform": "translate(-30px, 0px)" },
        },
        'right-center': {
          '0%': { "transform": "translate(-140px, 0px)" },
          '100%': { "transform": "translate(-30px, 0px)" },
        },
        'center-right': {
          '0%': { "transform": "translate(-30px, 0px)" },
          '100%': { "transform": "translate(-140px, 0px)" },
        },
        'left-right': {
          '0%': { "transform": "translate(50px, 0px)" },
          '100%': { "transform": "translate(-140px, 0px)" },
        },
      },
      animation: {
        'center-left': ' center-left  .3s linear forwards 1',
        'right-left': '  right-left   .3s linear forwards 1',
        'left-center': ' left-center  .3s linear forwards 1',
        'right-center': 'right-center .3s linear forwards 1',
        'center-right': 'center-right .3s linear forwards 1',
        'left-right': '  left-right   .3s linear forwards 1',
      }
    },
    fontFamily: {
      'lato': ['Lato']
    },
    
  },
  plugins: [],
}
